#!/usr/bin/env python3
import json
from pathlib import Path

def main():
    index = json.loads(Path('fdroid/repo/index-v1.json').read_text())
    apps = get_apps_html(index["apps"], index["packages"])
    layout = get_html(index["repo"], apps)
    print(layout)

def get_html(repo, apps):
    layout = Path('view/layout.html').read_text()
    return layout.format(repo=repo,appshtml=apps)

def get_apps_html(apps, packages):
    html = ""
    for app in apps:
        package = packages[app["packageName"]]
        html+= Path('view/app.html').read_text().format(app=app, package=package)
    return html

main()
