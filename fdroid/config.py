#!/usr/bin/env python3
repo_url = "https://llaq.gitlab.io/test-fdroid-repo"
repo_name = "Test F-DROID repo"
repo_icon = "g1.png"
repo_description = """
Blabla
"""
archive_older = 10
archive_url = "https://does.not.exists"
archive_name = "Rakshazi F-Droid"
archive_icon = "g1.png"
archive_description = "Does not exits"
repo_keyalias = "9294ac461a63"
keystore = "keystore.jks"
keydname = "CN=9294ac461a63, OU=F-Droid"
local_copy_dir = '/fdroid'
