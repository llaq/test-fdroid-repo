#!/usr/bin/env python3

import json
import re
import requests
import subprocess

def main():
  with open("apks.json") as file:
    apks = json.load(file)
  with open("cache/versions.json") as file:
    versions = json.load(file)
  for apk in apks:
    print("Preparing configuration for " + apk["name"] + "...")
    ver = ""
    ignore = False
    if "version" in apk:
      verObj = apk["version"]
      if "json" in verObj:
        ver = get_version_json(verObj["url"], verObj)
      elif "regex" in verObj:
        ver = get_version_regex(verObj["url"], verObj["regex"])
      if apk["name"] in versions and ver == versions[apk["name"]]:
        continue
      versions[apk["name"]] = ver
    print("Downloading " + apk["name"] + " " + ver)
    filename = (apk["name"] + "-" + ver).replace(" ", "_")
    if "ignoreErrors" in apk:
      ignore = apk["ignoreErrors"]
    if "architectures" in apk:
      for arch in apk["architectures"]:
        download(filename+ "-" + arch, apk["baseUrl"].format(arch=arch, ver=ver), ignore)
    else:
      download(filename, apk["baseUrl"].format(ver=ver), ignore)
  with open('cache/versions.json', 'w') as file:
    json.dump(versions, file, ensure_ascii=False)

def download(filename, download_url, ignore):
  retcode = subprocess.call(["wget", "-q", "-nc", "-O", "fdroid/repo/" + filename + ".apk", download_url])
  if not ignore and retcode != 0:
    raise Exception("Failed downloading " + download_url)
  print(filename + " done")

def get_version_regex(url, query):
  request = requests.get(url)
  regex = re.search(query, request.text)
  return regex.group(1)

def get_version_json(url, verObj):
  request = requests.get(url)
  version = request.json()
  if not isinstance(verObj["json"], list):
    version = version[verObj["json"]]
  else:
    for query_part in verObj["json"]:
      version = version[query_part]
  if "trim" in verObj.keys() and verObj["trim"] == True:
      version = version[1:]
  return version

if __name__ == "__main__":
  main()
